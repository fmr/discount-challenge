from discount_challenge import Client, Quote, Line


def test_three_door_sign_lines_receive_20_percent_on_all_door_sign_lines():

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='door_sign', unit_cost=10.0, quantity=2),
    ]).cost() == 20.0

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='door_sign', unit_cost=10.0, quantity=3),
    ]).cost() == 24.0

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='door_sign', unit_cost=10.0, quantity=3),
        Line(code='exterior_siren', unit_cost=100.0, quantity=1),
    ]).cost() == 124.0


def test_door_sign_with_escape_sign_receives_10_percent_off_entire_quote():

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='escape_sign', unit_cost=20.0, quantity=10),
    ]).cost() == 200.0

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='door_sign', unit_cost=10.0, quantity=1),
        Line(code='escape_sign', unit_cost=20.0, quantity=10),
    ]).cost() == 189.0


def test_specific_postcode_receives_free_fire_safety_audits():

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='fire_safety_audit', unit_cost=300.0, quantity=1),
    ]).cost() == 300.0

    assert Quote(client=Client(postcode=3152), lines=[
        Line(code='fire_safety_audit', unit_cost=300.0, quantity=1),
    ]).cost() == 0.0

    assert Quote(client=Client(postcode=3152), lines=[
        Line(code='escape_sign', unit_cost=20.0, quantity=10),
        Line(code='fire_safety_audit', unit_cost=300.0, quantity=1),
    ]).cost() == 200.0


def test_specific_postcode_receives_5_percent_off_entire_quote():

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='medium_extinguisher', unit_cost=150.0, quantity=2),
    ]).cost() == 300.0

    assert Quote(client=Client(postcode=3146), lines=[
        Line(code='medium_extinguisher', unit_cost=150.0, quantity=2),
    ]).cost() == 285.0

    assert Quote(client=Client(postcode=3146), lines=[
        Line(code='small_extinguisher', unit_cost=100.0, quantity=3),
        Line(code='medium_extinguisher', unit_cost=150.0, quantity=2),
    ]).cost() == 570.0


def test_specific_postcode_receive_50_percent_off_lemon_tree_lines():

    assert Quote(client=Client(postcode=3000), lines=[
        Line(code='lemon_tree', unit_cost=500.0, quantity=2),
    ]).cost() == 1000.0

    assert Quote(client=Client(postcode=4406), lines=[
        Line(code='lemon_tree', unit_cost=500.0, quantity=2),
    ]).cost() == 500.0

    assert Quote(client=Client(postcode=4406), lines=[
        Line(code='lemon_tree', unit_cost=500.0, quantity=2),
        Line(code='medium_extinguisher', unit_cost=150.0, quantity=1),
    ]).cost() == 650.0
