from abc import ABCMeta, abstractmethod

# To complete:
#
#  - Create a structure for representing generic Discount Rules to Quotes
#    based on test cases 1 through 5 described below
#
#  - Awareness of the Discount Rules should occur during calculating the quote
#    cost
#
#  - All DISCOUNT_RULES should be active on all tests - there are no
#    contradictory test cases.
#
#  - The solution should be extensible: we'd like to add some more rules later
#    on. When successful, all assertions will pass


class DiscountRule:

    def __init__(self, condition, multiplier, target='TOTAL'):
        self.condition = condition
        self.multiplier = multiplier
        self.target = target  # benefit target


def line_code_quantity_condition(code, min_qty):
    """
    Returns a function that checks whether quote lines have at least min_qty
    number of a given line code
    """
    # condition: 3 door_sign lines
    # benefit: all door sign lines get a 20% discount
    return lambda quote: sum(
        l.quantity for l in quote.lines if l.code == code) >= min_qty


# signature: (codes)
def line_code_combo_condition(codes=['door_sign', 'escape_sign']):
    """
    Returns a function that checks that the given codes all appear in the quote
    lines
    """
    # condition: has at least 1 door_sign line, 1 escape_sign line
    # benefit: 10% discount off total
    return lambda quote: set(codes) <= set(l.code for l in quote.lines)


# signature: (postcode)
def postcode_condition(postcode):
    """
    Returns a function that checks whether the client is from a given postcode
    """
    return lambda quote: quote.client.postcode == postcode


class DiscountStrategy(metaclass=ABCMeta):
    """
    Abstract base class for the different discount strategies
    """

    @abstractmethod
    def get_rule(self):
        """
        Returns a DiscountRule object
        """
        pass


class LineCodeDiscount(DiscountStrategy):
    """
    Discount strategy for when a given code appears more than
    the minimum amount specified, all the line items with that code receives
    a discount.

    Require: line code, minimum quantity, multiplier
    Benefit: discount applies to all lines matching the given line_code
    """

    def __init__(self, **kwargs):
        self.line_code = kwargs.get('line_code')
        self.min_qty = kwargs.get('min_qty')
        self.multiplier = kwargs.get('multiplier')
        self.condition = line_code_quantity_condition

    def get_rule(self):
        return DiscountRule(
            self.condition(self.line_code, self.min_qty),
            self.multiplier,
            self.line_code)


class LineCodeComboDiscount(DiscountStrategy):
    """
    Represents a discount strategy that checks for a combination of line codes,
    and a discount is applied to the total cost
    Require: line codes, multiplier
    Benefit: discount applies to the total cost
    """

    def __init__(self, **kwargs):
        self.line_codes = kwargs.get('line_codes')
        self.multiplier = kwargs.get('multiplier')
        self.condition = line_code_combo_condition

    def get_rule(self):
        return DiscountRule(
            self.condition(self.line_codes),
            self.multiplier)


class PostcodeDiscount(DiscountStrategy):
    """
    Postcode-based discount strategy which can be applied to a target line code
    or the total amount
    Require: postcode, target, multiplier
    Benefit: discount for a given postcode is applied on lines of a given
    target, or the total cost if target is not specified
    """

    def __init__(self, **kwargs):
        self.postcode = kwargs.get('postcode')
        self.target = kwargs.get('target', 'TOTAL')
        self.multiplier = kwargs.get('multiplier')
        self.condition = postcode_condition

    def get_rule(self):
        return DiscountRule(
            self.condition(self.postcode),
            self.multiplier,
            self.target)


DISCOUNT_RULES = (
    # rule #1
    # condition: 3 or more door_sign lines
    # target: ['door_sign']
    # benefit: receive 20% off their 'door_sign' lines

    # {
    #     'code': door_sign,
    #     'min_qty': 3,
    #     'target': 'door_sign',
    #     'multiplier': 0.8
    # }
    LineCodeDiscount(
        line_code='door_sign', min_qty=3, multiplier=0.8).get_rule(),

    # rule #2:
    # condition: at least one door_sign line and one escape_sign line
    # benefit: receive 10% off the TOTAL

    # {'codes': ['door_sign', 'escape_sign'], 'multiplier': 0.9}
    LineCodeComboDiscount(
        line_codes=['door_sign', 'escape_sign'], multiplier=0.9).get_rule(),

    # rule #3:
    # condition: postcode is 3152
    # benefit: all fire_safety_audit lines are free (multiplier: 0)

    # {'postcode': '3152', 'target': 'fire_safety_audit', 'multiplier': 0}
    PostcodeDiscount(
        postcode=3152, target='fire_safety_audit', multiplier=0).get_rule(),

    # rule #4:
    # condition: postcode is 3146
    # benefit: receive 5% off the total

    # {'postcode': '3146', 'multiplier': 0.95}
    PostcodeDiscount(postcode=3146, multiplier=0.95).get_rule(),

    # rule #5: by postcode (4406)
    # when a quote is made for a client in postcode 4406 for containing
    # 'lemon_tree' lines, they receive 50% off the 'lemon_tree' lines

    # {'postcode': '4406', 'target': 'lemon_tree', 'multiplier': 0.5}
    PostcodeDiscount(
        postcode=4406, target='lemon_tree', multiplier=0.5).get_rule()
)

# Acceptable assumptions:
# - the given line items are unique by 'code'


class Client:
    """
    Discount beneficiary
    """

    def __init__(self, postcode):
        self.postcode = postcode

    def __repr__(self):
        return str(self.postcode)


class Line:
    """
    Quote line
    """

    def __init__(self, code, unit_cost, quantity=1):
        self.code = code
        self.unit_cost = unit_cost
        self.quantity = quantity

    def cost(self):
        return self.unit_cost * self.quantity

    def __repr__(self):
        return str((self.code, self.quantity, self.unit_cost))


class Quote:
    """
    Calculates the total cost for a given set of lines
    """

    def __init__(self, client=None, lines=[]):
        self.client = client
        self.lines = lines

    def compute_discounts(self):
        discounts = dict((l.code, 1.0) for l in self.lines)
        discounts['TOTAL'] = 1.0
        for rule in DISCOUNT_RULES:
            if rule.condition(self):
                discounts[rule.target] *= rule.multiplier
        return discounts

    def cost(self):
        discounts = self.compute_discounts()
        return sum(
            [l.cost() * discounts[l.code] for l in self.lines]
        ) * discounts['TOTAL']
